//
//  Weather.swift
//  WeatherLocal
//
//  Created by André Feliciano on 16/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import Foundation

struct Weather {

    let day             : String    ///  Dia
    let dayWeek         : String    ///  Dia da semana
    let summary         : String    ///  Resumo
    let sunriseTime     : String    ///  Nascer do sol
    let sunsetTime      : String    ///  Pôr do sol
    let humidity        : String    ///  Humidade
    let pressure        : String    ///  Pressão
    let windSpeed       : String    ///  Velocidade do vento
    let visibility      : String    ///  Visibilidade
    let temperatureMin  : String    ///  Temperatura mínima
    let temperatureMax  : String    ///  Temperatura máxima
    
    
    init(
        day             : Int,
        dayWeek         : Int,
        summary         : String,
        sunriseTime     : Int,
        sunsetTime      : Int,
        humidity        : Double,
        pressure        : Double,
        windSpeed       : Double,
        visibility      : Double,
        temperatureMin  : Double,
        temperatureMax  : Double
        ) {
        
        let date = Date()
        
        self.day = date.showDate(timeInterval: day, dateStyle: "dd/MM")
        self.dayWeek = date.showWeek(timeInterval: dayWeek)
        self.summary = summary
        self.sunriseTime = "\(sunriseTime)"
        self.sunsetTime = "\(sunsetTime)"
        self.humidity = "\(Int(humidity * 100))%"
        self.pressure = "\(Int(pressure.rounded())) hPa"
        self.windSpeed = "\(Int(windSpeed.rounded())) Km/h"
        self.visibility = "\(Int(visibility.rounded())) Km"
        self.temperatureMin = "\(Int(temperatureMin.rounded()))ºC"
        self.temperatureMax = "\(Int(temperatureMax.rounded()))ºC"
    }
}
