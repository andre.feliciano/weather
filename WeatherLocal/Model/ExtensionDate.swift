//
//  ExtensionDate.swift
//  WeatherLocal
//
//  Created by André Feliciano on 13/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import Foundation


extension Date {
    
    
    /// Return day of month, based on the timeinterval value
    public func showDay(timeInterval: Int) -> Int {
        let calendar = Calendar.current
        let day = calendar.component(.day, from: Date(timeIntervalSince1970: TimeInterval(timeInterval)))
        
        return day
    }
    
    
    /// Return full week of month, based on the timeinterval value
    func showWeek(timeInterval: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.pt_BR
        dateFormatter.dateFormat = "EEEE"
        let week = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(timeInterval)))
        
        return week
    }
    
    
    /// Return date, based on the timeinterval value
    func showDate(timeInterval: Int, dateStyle: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.pt_BR
        dateFormatter.dateStyle = dateStyle
        let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(timeInterval)))
        
        return date
    }
    
    
    /// Return custom date, based on the timeinterval value
    func showDate(timeInterval: Int, dateStyle: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.pt_BR
        dateFormatter.dateFormat = dateStyle
        let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(timeInterval)))
        
        return date
    }
    
}
