//
//  REST.swift
//  WeatherLocal
//
//  Created by André Feliciano on 11/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import Foundation

enum StatusCode: Int {
    case ok = 200
}
