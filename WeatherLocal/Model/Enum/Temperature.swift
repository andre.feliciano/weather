//
//  Temperature.swift
//  WeatherLocal
//
//  Created by André Feliciano on 26/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import Foundation


enum TemperatureScale: String {
    case celsius = "ºC"
    case kelvin = "K"
    case fahrenheit = "°F"
}

enum TemperatureField: String {
    case currently = "currently"
    case temperature = "temperature"
    case daily = "daily"
    case data = "data"
    case time = "time"
    case summary = "summary"
    case sunriseTime = "sunriseTime"
    case sunsetTime = "sunsetTime"
    case humidity = "humidity"
    case pressure = "pressure"
    case windSpeed = "windSpeed"
    case visibility = "visibility"
    case temperatureMin = "temperatureMin"
    case temperatureMax = "temperatureMax"
}
