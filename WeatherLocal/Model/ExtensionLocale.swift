//
//  ExtensionLocale.swift
//  WeatherLocal
//
//  Created by André Feliciano on 13/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import Foundation


extension Locale {
    static let pt_BR = Locale(identifier: "pt_BR")
}
