//
//  WeatherTableViewCell.swift
//  WeatherLocal
//
//  Created by André Feliciano on 18/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    let border: CGFloat = 1.0
    let corner: CGFloat = 10
    
    @IBOutlet weak var dayWeek: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var temperatureMax: UILabel!
    @IBOutlet weak var temperatureMin: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var sunriseTime: UILabel!
    @IBOutlet weak var sunsetTime: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var visibility: UILabel!
    @IBOutlet weak var viewTemperature: UIView! {
        didSet {
            viewTemperature.layer.cornerRadius = corner
            viewTemperature.layer.borderColor = UIColor.black.cgColor
            viewTemperature.layer.borderWidth = border
        }
    }
    
    
    
    
    func setupCell(weather: Weather) {
        
        dayWeek.text = weather.dayWeek
        date.text = weather.day
        temperatureMax.text = weather.temperatureMax
        temperatureMin.text = weather.temperatureMin
        summary.text = weather.summary
        humidity.text = weather.humidity
        pressure.text = weather.pressure
        windSpeed.text = weather.windSpeed
        visibility.text = weather.visibility
    }
    
}
