//
//  ViewController.swift
//  WeatherLocal
//
//  Created by André Feliciano on 11/01/20.
//  Copyright © 2020 André Feliciano. All rights reserved.
//


import CoreLocation
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var errorLocation: UILabel!
    @IBOutlet weak var weatherTable: UITableView!
    
    var latitude  = String()
    var longitude = String()
    private var weatherList: [Weather] = []
    
    // Used to start getting the users location
    let locationManager = CLLocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For use when the app is open & in the background
        locationManager.requestAlwaysAuthorization()
        
        // For use when the app is open
        locationManager.requestWhenInUseAuthorization()
        
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.startUpdatingLocation()
        }
        
        weatherTable.dataSource = self
        errorLocation.isHidden = false
        weatherTable.isHidden = true
    }
    
    
    
    func errorGetLocation(scale: String) {
        DispatchQueue.main.async {
            self.temperature.text = "0" + scale
            self.errorLocation.text = "Descuple, não foi possivel obter sua localização"
        }
    }
    
    
    
    func loadWeather() {
        if longitude != "" && latitude != "" {
            errorLocation.isHidden = true
            weatherTable.isHidden = false
            getWheather(latitude: self.latitude, longitude: self.longitude)
            
            self.locationManager.stopUpdatingLocation()
        }
        
        longitude = ""
        latitude = ""
    }
    
    
    /// Get weather data
    private func getWheather(latitude: String, longitude: String) {
        let baseURL = "https://api.darksky.net/forecast"
        let key = "73e037dd8ecda9d090ae2dc239e1a461"
        let units = "units=auto"
        let lang = "lang=pt"
        let exclude = "exclude=hourly"
        let setParamns = "\(units)&\(lang)&\(exclude)"
        
        guard let url = URL(string: "\(baseURL)/\(key)/\(latitude),\(longitude)?\(setParamns)") else { return }

        let celcius = TemperatureScale.celsius.rawValue
        let tarefa = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error == nil {
                
                guard let resposne = response as? HTTPURLResponse else { return }
                
                if resposne.statusCode == StatusCode.ok.rawValue {
                    if let data = data {
                        
                        do {
                            if let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: Any] {
                                
                                guard let currently = jsonObject[TemperatureField.currently.rawValue] as? [AnyHashable:Any], let temperature = currently[TemperatureField.temperature.rawValue] as? Double else {
                                    return
                                }
                                
                                DispatchQueue.main.async {
                                    let temperature = Int(temperature.rounded())
                                    self.temperature.text = String(temperature) + celcius
                                }
                                
                                guard let daily = jsonObject[TemperatureField.daily.rawValue] as? [AnyHashable:Any], let dataWeather = daily[TemperatureField.data.rawValue] as? [[String: AnyObject]] else {
                                    return
                                }
                                
                                self.weatherList.removeAll()
                                
                                for weather in dataWeather {
                                    
                                    guard let time = weather[TemperatureField.time.rawValue] as? Int else { return }
                                    guard let summary = weather[TemperatureField.summary.rawValue] as? String else { return }
                                    guard let sunriseTime = weather[TemperatureField.sunsetTime.rawValue] as? Int else { return }
                                    guard let sunsetTime = weather[TemperatureField.sunsetTime.rawValue] as? Int else { return }
                                    guard let humidity = weather[TemperatureField.humidity.rawValue] as? Double else { return }
                                    guard let pressure = weather[TemperatureField.pressure.rawValue] as? Double else { return }
                                    guard let windSpeed = weather[TemperatureField.windSpeed.rawValue] as? Double else { return }
                                    guard let visibility = weather[TemperatureField.visibility.rawValue] as? Double else { return }
                                    guard let temperatureMin = weather[TemperatureField.temperatureMin.rawValue] as? Double else { return }
                                    guard let temperatureMax = weather[TemperatureField.temperatureMax.rawValue] as? Double else { return }
                                    
                                    
                                    let weatherDaily = Weather(day: time, dayWeek: time, summary: summary, sunriseTime: sunriseTime, sunsetTime: sunsetTime, humidity: humidity, pressure: pressure, windSpeed: windSpeed, visibility: visibility, temperatureMin: temperatureMin, temperatureMax: temperatureMax)
                                    
                                    self.weatherList.append(weatherDaily)
                                }
                                
                                self.weatherList.removeFirst()
                                
                                DispatchQueue.main.async {
                                    self.weatherTable.reloadData()
                                }
                                
                            }
                        } catch {
                            print("Erro ao formatar o retorno")
                        }
                        
                    }
                } else {
                    self.errorGetLocation(scale: celcius)
                }
                
            } else {
                self.errorGetLocation(scale: celcius)
            }
        }
        tarefa.resume()
        
    }
    
    
    // Show the popup to the user if we have been deined access
    private func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Acesso à localização em segundo plano está desativado",
                                                message: "Para fornecer a previsão do tempo, precisamos da sua localização",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Abrir Ajustes", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}





// MARK: - CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
    
    // Print out the location to the console
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            let coordinates = location.coordinate
            latitude = String(coordinates.latitude)
            longitude = String(coordinates.longitude)
        }
        loadWeather()
    }
    
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
}






// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let celula = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherTableViewCell else {
            return UITableViewCell()
        }
        
        celula.setupCell(weather: weatherList[indexPath.row])
        return celula
    }
}
